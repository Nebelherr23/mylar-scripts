# mylar-scripts

My repository of scripts for [mylar3](https://github.com/mylar3/mylar3)

## What does it contain?

- A pure-python script webp conversion script, that you can run both via mylar's post-processing script or as a command line script (convert-webp)
