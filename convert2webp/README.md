## Convert images in CBR/CBZ files to webp

This has been written for use in the [lsio](https://linuxserver.io) docker image, but works outside it (and as a standalone) as well. There are some things to regard.

### General info

This script is built on the assumption of cwebp, file and 7z being globally available on run. The docker image has a mechanism to get this working (see [LSIO docs](https://docs.linuxserver.io/general/container-customization#custom-scripts) and below for details)

#### mylar3 - script for the official docker image

Mount a script into the container using ```/path/to/scriptdir:/custom-cont-init.d:ro``` (please keep the :ro, otherwise the script will not be run)

In the script, install the following packages (`apk add --no-cache`):
 * libwebp-tools (required tools for webp conversion)
 * p7zip (unpack the CBZ/CBR file for conversion)
 * file (required to identify archive type via mime magic)

 Also, create a work directory and `chown abc:abc` it.

#### Other systems

Basically, what I said for the docker image, counts for other setups as well. Make sure the packages mentioned above are available.
